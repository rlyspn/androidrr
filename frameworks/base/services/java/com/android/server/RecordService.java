/*RecordService.java */
package com.android.server;
import android.content.Context;
import android.os.Handler;
import android.os.IRecordService;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.util.Log;
import android.view.TouchLogger; 
import android.view.MotionEvent; 
import android.view.KeyEvent; 

public class RecordService extends IRecordService.Stub {

    private static final String TAG = "RecordService";
    private RecordWorkerThread mWorker;
    private RecordWorkerHandler mHandler;
    private Context mContext;
	private boolean mRecording; 
	private boolean mOn; 
	private String defaultFile = "TouchLog.log";
	private int defaultSize = 0;

	private TouchLogger mTouchLogger; 	


    public RecordService(Context context) {
        super();
        mContext = context;
        // mWorker = new RecordWorkerThread("RecordServiceWorker");
        // mWorker.start();
		
//		turnOn(); 
//		start(); 

		mTouchLogger = TouchLogger.getTouchLogger(defaultFile, defaultSize, mContext); 
        // Log.i(TAG, "Spawned Record worker thread");
    }
 
    public void turnOn() {
        Log.i(TAG, "Turning Log On");
        mOn = true; 
    }
    
	public void turnOff() {
        Log.i(TAG, "Turning Log Off");
        mOn = false; 
		mRecording = false; 
    }

	public boolean start() { 
		Log.i(TAG, "Starting Record"); 
		boolean success = true; 
		if ( mOn ) {
			mRecording = true; 
			Log.i(TAG, "Recording..."); 
		} 
		else { 
			success = false; 
		 	Log.i(TAG, "Record Service is off"); 
		}
		return success; 
	} 

	public boolean stop() { 
		Log.i(TAG, "Stopping Record"); 
		boolean success = true; 

		if ( mOn ) {
			mRecording = false; 
			Log.i(TAG, "Recording..."); 
		} 
		else { 
			success = false; 
		 	Log.i(TAG, "Record Service is off"); 
		}
		return success; 
	}
	
	public boolean isRecording() { 
		return mRecording; 		
	}

	public boolean addEvent(MotionEvent event) { 
		boolean success = true ; 

		Log.i(TAG, "Adding Event from Service");
		success = mTouchLogger.addEvent(event); 
		
		return success; 
	} 

    public boolean addKeyEvent(KeyEvent event) {
        Log.i(TAG, "Adding TouchEvent from service.");
        return mTouchLogger.addKeyEvent(event);
    }

    private class RecordWorkerThread extends Thread {
        public RecordWorkerThread(String name) {
            super(name);
        }

        public void run() {
            Looper.prepare();
            mHandler = new RecordWorkerHandler();
            Looper.loop();
        }
    }
 
    private class RecordWorkerHandler extends Handler {
        private static final int MESSAGE_SET = 0;
        
		@Override
        public void handleMessage(Message msg) {
            try {
                if (msg.what == MESSAGE_SET) {
                    Log.i(TAG, "Record Switch: " + msg.arg1);
                }
            } catch (Exception e) {
                // Log, don't crash!
                Log.e(TAG, "Exception in RecordWorkerHandler.handleMessage:", e);
            }
        }
    }
}
