/**
 * Added by Riley Spahn
 * Date: 4/10/2013
 */

package android.view;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Semaphore;

public class TouchLogger implements Runnable {
    public static final String TAG = "TouchLogger";

    private String touchLogFileName = null;

    private static TouchLogger touchLogger = null;

    private ConcurrentLinkedQueue<LoggerEvent> eventQueue;

    private int queueLimit;

    private Semaphore qTex;

    private Context context;

    private ArrayList<MotionEvent> prevEvents;

    private final String DEFAULT_DIR = "/data/tmp/";

    /*
     * @param fileName - the name into which the log will be written. If null
     * the log will be printed to logcat.
     * @param queueLimit - the size limit for the queue before writing the file.
     * @param context - the context from which the events are being logged.
     */
    public static TouchLogger getTouchLogger(String fileName, int queueLimit, Context context) {
        if (touchLogger == null)
            touchLogger = new TouchLogger(fileName, queueLimit, context);
        return touchLogger;
    }

    /*
     * @param context - the context from which events are being logged.
     * queueLimit defaults to 1 and the file defaults to logcat.
     */
    public static TouchLogger getTouchLogger(Context context) {
        if (touchLogger == null)
            touchLogger = new TouchLogger(null, 1, context);
        return touchLogger;
    }

    /*
     * Flushes the queue to logs if the limit is surpassed.
     */
    public void run() {
        try {
            qTex.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
            return;
        }
        flushQueue();
        qTex.release();
    }

    /*
     * @param fileName - the name into which the log will be written. If null
     * the log will be printed to logcat.
     * @param queueLimit - the size limit for the queue before writing the file.
     * @param context - the context from which the events are being logged.
     */
    private TouchLogger(String fileName, int queueLimit, Context context) {
        qTex = new Semaphore(1);
        touchLogFileName = fileName;
        this.queueLimit = queueLimit;
        eventQueue = new ConcurrentLinkedQueue<LoggerEvent>();
        this.context = context;
        prevEvents = new ArrayList<MotionEvent>();
    }

    private boolean pushMoveEvent() {
        Log.d(TAG, "Pushing MoveEvent.");
        LoggerPoint pointArray[];
        boolean retVal = false;
        int eventCount = prevEvents.size();
        MotionEvent lastEvent = prevEvents.get(eventCount - 1);
        float xPrecision = lastEvent.getXPrecision();
        float yPrecision = lastEvent.getYPrecision();

        pointArray = new LoggerPoint[prevEvents.size() + 1];
        Log.d(TAG, "precision = (" + xPrecision + ", " + yPrecision + ")");
        Log.d(TAG, "pointCount = " + pointArray.length);
        for (int i = 0; i < prevEvents.size(); i++) {
            Log.d(TAG, "Adding point: " + i);
            MotionEvent ce = prevEvents.get(i);
            pointArray[i] = new LoggerPoint(ce.getRawX(), ce.getRawY());
        }

        Log.d(TAG, "prevEventCount = " + prevEvents.size());
        for (int i = 0; i < prevEvents.size(); i++) {
            MotionEvent ev = prevEvents.get(i);
            int historySize = ev.getHistorySize();
            int pointerCount = ev.getPointerCount();
            Log.d(TAG, "historySize = " + historySize);
            Log.d(TAG, "pointerCount = " + pointerCount);
            for (int h = 0; h < historySize; h++) {
                Log.d(TAG, "At time " + ev.getHistoricalEventTime(h));
                for (int p = 0; p < pointerCount; p++) {
                    Log.d(TAG, "  pointer " + ev.getPointerId(p) + " :" + ev.getHistoricalX(p, h)
                            + " " + ev.getHistoricalY(p, h));
                    Log.d(TAG, "  pointer " + ev.getPointerId(p) + " :" + ev.getHistoricalX(p, h)
                            * xPrecision + " " + ev.getHistoricalY(p, h) * yPrecision);
                }
            }
        }

        LoggerDragEvent newEvent = new LoggerDragEvent(pointArray,
                prevEvents.get(0).getEventTime(), prevEvents.get(eventCount - 1).getEventTime());

        prevEvents = new ArrayList<MotionEvent>();
        try {
            qTex.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
            Log.e(TAG, "Interrupted while acquiring the list semaphore.");
            return false;
        }
        // retVal = eventQueue.add(newEvent);
        qTex.release();
        if (eventQueue.size() >= queueLimit) {
            // (new Thread(touchLogger)).start();
        }
        return retVal;
    }

    /*
     * Handles an action up event. This may signify the end of a touch event or
     * a keypress event.
     * @param event - the ACTION_UP event to process.
     * @return - true if the event was processed correctly.
     */
    private boolean handleActionUp(MotionEvent event) {
        Log.d(TAG, "handling up event, prevEventssize = " + prevEvents.size());
        boolean retVal = false;
        if (prevEvents.size() == 1) { /* Touch Event */
            LoggerTouchEvent newEvent = new LoggerTouchEvent(event.getRawX(), event.getRawY(),
                    prevEvents.get(0).getEventTime(), event.getEventTime());

            prevEvents = new ArrayList<MotionEvent>();
            try {
                qTex.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
                Log.e(TAG, "Interrupted while acquiring the list semaphore.");
                return false;
            }
            retVal = eventQueue.add(newEvent);
            qTex.release();
            if (eventQueue.size() >= queueLimit) {
                (new Thread(touchLogger)).start();
            }

        } else if (prevEvents.size() == 2) {
            /* This may be a key press event. */
        } else if (prevEvents.size() > 2) { /* Drag Event */
            prevEvents.add(event);
            retVal = pushMoveEvent();
            prevEvents = new ArrayList<MotionEvent>();
        } else {
            prevEvents = new ArrayList<MotionEvent>();
            /* Error */
        }
        return retVal;
    }

    /*
     * Handles a down event. If the down event is preceded by another down event
     * then it will be recorded as touch event.
     * @param event - the event to be handled.
     * @return - true succesfully handled event.
     */
    private boolean handleActionDown(MotionEvent event) {
        boolean retVal = true;
        Log.d(TAG, "handling down event, prevEventssize = " + prevEvents.size());
        if (prevEvents.size() == 1) {
            /* This will be a key press. */
            if (prevEvents.get(0).getAction() == MotionEvent.ACTION_DOWN) {
                MotionEvent lastEvent = prevEvents.get(0);
                LoggerTouchEvent newEvent = new LoggerTouchEvent(lastEvent.getRawX(),
                        lastEvent.getRawY(), lastEvent.getEventTime(), event.getEventTime());
                retVal = eventQueue.add(newEvent);
                if (eventQueue.size() >= queueLimit) {
                    (new Thread(touchLogger)).start();
                }
                prevEvents = new ArrayList<MotionEvent>();
            }
        } else if (prevEvents.size() > 1) {
            pushMoveEvent();
            prevEvents = new ArrayList<MotionEvent>();
            retVal = true;
        }
        prevEvents.add(event);
        return retVal;
    }

    /*
     * Handles adding event to the queue where the event type is ACTION_MOVE
     * Some number of ACTION_MOVE events occur between an ACTION_DOWN and
     * ACTION_CANCEL event.
     * @param event - the event to add to the queue.
     * @return - true if successfully added.
     */
    private boolean handleActionMove(MotionEvent event) {
        Log.d(TAG, "Handling move event.");
        Log.d(TAG, "HANDLE: historySize = " + event.getHistorySize());
        Log.d(TAG, "HANDLE: pointerCoung = " + event.getPointerCount());
        //prevEvents.add(event);
        return true;
    }

    /*
     * Handles an ACTION_CANCEL event that may occur at the end of a drag.
     * @param event - The ACTION_CANCEL event to be processed
     * @return - true if successfully added.
     */
    private boolean handleActionCancel(MotionEvent event) {
        /* TODO */

        return true;
    }

    public boolean addKeyEvent(KeyEvent event) {
        int keyCode = event.getKeyCode();
        long down = event.getEventTime();
        long up = down;

        LoggerKeyEvent newEvent = new LoggerKeyEvent(down, up, keyCode);
        
        return eventQueue.add(newEvent);
    }

    /*
     * Adds an event to the queue of events to process.
     * @param event - the event to add.
     * @return - returns true of successfully added and false otherwise.
     */
    public boolean addEvent(MotionEvent event) {
        boolean ret_bool;
        Log.d(TAG, "Source = " + event.getSource());
        Log.d(TAG, "Handlind action: " + event.getAction());
        Log.d(TAG, "queueSize = " + eventQueue.size());
        Log.d(TAG, event.getDownTime() + " " + " " + event.getEventTime() + "\n");
        if (event.getSource() == InputDevice.SOURCE_TOUCHSCREEN) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    ret_bool = handleActionDown(event);
                    break;
                case MotionEvent.ACTION_MOVE:
                    ret_bool = handleActionMove(event);
                    break;
                case MotionEvent.ACTION_UP:
                    ret_bool = handleActionUp(event);
                    break;
                case MotionEvent.ACTION_CANCEL:
                    ret_bool = handleActionCancel(event);
                default:
                    Log.e(TAG, "Action not supported: " + event.getAction());
                    ret_bool = false;
            }
        } else {
            Log.e(TAG, "Error: We currently only handle the touch screen.");
            ret_bool = false;
        }
        return ret_bool;
    }

    /*
     * Adds a new event to the queue. It will flush the queue if the limit is
     * passed.
     * @param x - x coordinate of the event.
     * @param y - y coordinate of the event.
     * @param down - time which the event was started.
     * @param up - time which the event finished.
     */
    public boolean addEvent(float x, float y, float down, float up) {
        LoggerTouchEvent newEvent = new LoggerTouchEvent(x, y, down, up);
        try {
            qTex.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
            Log.e(TAG, "Interrupted while acquiring the list semaphore.");
            return false;
        }
        boolean retVal = eventQueue.add(newEvent);
        qTex.release();
        if (eventQueue.size() >= queueLimit) {
            (new Thread(touchLogger)).start();
        }
        return retVal;
    }

    /*
     * Adds an event to the queue and flushes the queue if needed.
     * @param event - event to be logged.
     */
    public boolean addEvent(LoggerTouchEvent event) {
        try {
            qTex.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
            Log.e(TAG, "Interrupted while acquiring the list semaphore.");
            return false;
        }
        boolean retVal = eventQueue.add(event);
        qTex.release();
        if (eventQueue.size() >= queueLimit) {
            (new Thread(touchLogger)).start();
        }
        return retVal;
    }


    /*
     * Writes the queue out to a file or logcat.
     * @param filename - the name of the file to flush the queue to or null to
     * flush to logcat.
     */
    private void flushToFile(String filename) {
        Log.d(TAG, "Flushing to file: " + filename);
        FileOutputStream outStream = null;
        try {
            if (context == null) {
                Log.d(TAG, "Context is null.");
                new File(DEFAULT_DIR).mkdir();
                File newFile = new File(DEFAULT_DIR, filename);
                outStream = new FileOutputStream(newFile, true);
            } else {
                // outStream = context.openFileOutput(filename,
                // Context.MODE_APPEND);
                new File(DEFAULT_DIR).mkdir();
                File newFile = new File(DEFAULT_DIR, filename);
                outStream = new FileOutputStream(newFile, true);
            }
            while (eventQueue.size() > 0) {
                outStream.write(eventQueue.poll().toString().getBytes());
                outStream.write('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (outStream != null)
                    outStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /*
     * Writes the log to logCat.
     */
    private void flushToLogcat() {
        while (eventQueue.size() > 0)
            Log.d(TAG, eventQueue.poll().toString());
    }

    /*
     * Flushes the queue to either logcat or a file.
     */
    private void flushQueue() {
        if (touchLogFileName == null)
            flushToLogcat();
        else
            flushToFile(touchLogFileName);
    }

    public void setQueueLimit(int queueLimit) {
        this.queueLimit = queueLimit;
    }

    public int getQueueLimit() {
        return queueLimit;
    }

    public void setTouchLogFileName(String touchLogFileName) {
        this.touchLogFileName = touchLogFileName;
    }

    public String getTouchLogFileName() {
        return touchLogFileName;
    }
}
