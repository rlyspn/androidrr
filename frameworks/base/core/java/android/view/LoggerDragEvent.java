
package android.view;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;

public class LoggerDragEvent implements LoggerEvent {
    private LoggerPoint[] points;
    private float down, up;

    public LoggerDragEvent(LoggerPoint[] points, float down, float up) {
        this.points = points;
        this.up = up;
        this.down = down;
    }

    public JSONObject getJSONObject() {
        JSONObject obj = new JSONObject();
        JSONArray ary = new JSONArray();

        for (int i = 0; i < points.length; i++) {
            ary.put(points[i].getJSONObject());
        }
        try {
            obj = obj.put("type", "drag");
            obj = obj.put("down", down);
            obj = obj.put("up", up);
            obj = obj.put("points", ary);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return obj;
    }

    public String toString() {
        return "";
        // return getJSONObject().toString();
    }
}
