/**
 * Added by Riley Spahn
 * Date: 4/10/2013
 */

package android.view;

import java.lang.StringBuilder;
import org.json.JSONObject;
import org.json.JSONException;

public class LoggerTouchEvent implements LoggerEvent {
    private float x, y, down, up;

    public LoggerTouchEvent(float x, float y, float down, float up) {
        this.x = x;
        this.y = y;
        this.down = down;
        this.up = up;
    }

    public LoggerTouchEvent() {
        x = -1;
        y = -1;
        down = -1;
        up = -1;
    }

    public String toString() {
        try {
            JSONObject jobj = new JSONObject();
            jobj.put("type", "touch");
            jobj.put("x", x);
            jobj.put("y", y);
            jobj.put("up", up);
            jobj.put("down", down);
            return jobj.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            StringBuilder sb = new StringBuilder();
            sb = sb.append("{").append("x:").append(x).append(",y:").append(y)
                    .append(",up:").append(up).append(",down:").append(down)
                    .append("}");
            return sb.toString();
        }
    }

    public void setX(float x) {
        this.x = x;
    }

    public void setY(float y) {
        this.y = y;
    }

    public void setDown(float down) {
        this.down = down;
    }

    public void setUp(float up) {
        this.up = up;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getUp() {
        return up;
    }

    public float getDown() {
        return down;
    }
}
