package android.view;

public class LoggerKeyEvent implements LoggerEvent {
    private float down,up;
    private int keyCode;
    private String keyCodeString;

    public LoggerKeyEvent(float down, float up, int keyCode) {
        this.down = down;
        this.up = up;
        this.keyCode = keyCode;
        this.keyCodeString = KeyEvent.keyCodeToString(keyCode);
    }

    public float getDown() {
        return down;
    }
    public float getUp() {
        return up;
    }
    public String getKeyCodeString() {
        return keyCodeString;
    }
    public int getKeyCode() {
        return keyCode;
    }
    /*
     * Example:
     * {"type": "press", "keys": [{"key": "KEYCODE_SHIFT_LEFT"},
     * {"key": "KEYCODE_Y"} ], "down": 123123, "up": 123124 }
     */
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb = sb.append("{\"type\":\"press\",\"keys\":[{\"key\":\"").append(
                keyCodeString).append("\"}],\"down\":").append(down).append(
                    ",\"up\":").append(up).append("}");
        
        return sb.toString();
    }
}
