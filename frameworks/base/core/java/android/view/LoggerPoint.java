
package android.view;

import org.json.JSONException;
import org.json.JSONObject;

public class LoggerPoint {
    private float x, y;

    public LoggerPoint(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public JSONObject getJSONObject() {
        JSONObject obj = new JSONObject();
        try {
            obj = obj.put("y", y);
            obj = obj.put("x", x);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return obj;
    }

    public String toString() {
        return getJSONObject().toString();
    }
}
