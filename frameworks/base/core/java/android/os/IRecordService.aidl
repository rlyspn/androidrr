/*
* aidl file : frameworks/base/core/java/android/os/IRecordService.aidl
* @author: Dmitriy Gromov 
*/

package android.os;
import android.view.MotionEvent; 
import android.view.KeyEvent; 

interface IRecordService {
	void turnOn();
	
	void turnOff(); 

	boolean addEvent(in MotionEvent event);
	
	boolean start(); 
	
	boolean stop(); 
	
	boolean isRecording(); 
    
    boolean addKeyEvent(in KeyEvent event);
}

